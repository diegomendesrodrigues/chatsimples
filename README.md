# chatsimples

**ChatSimples**

O usuário final (Diego), além de um '*bot*' (Julia) que responde as perguntas feitas. As perguntas foram programadas com expressões regulares, sendo que, para cada pergunta, existe uma resposta que será exibida.

As perguntas foram programadas com expressões regulares, sendo que, para cada pergunta, existe uma resposta que será exibida.

O Chat é simples, respondendo perguntas como:
+ Tudo bem?
+ Como está?
+ Como anda?
+ Qual a sua idade?
+ Sua idade?
+ Quantos anos você tem?
+ Quantos anos?
+ Qual seu nome?
+ Onde mora?
+ Onde vive?
+ Onde você mora?
+ Onde estuda?
+ Qual faculdade?
+ Qual faculdade você faz?
+ Que curso?
+ Qual curso?
+ O que gosta de fazer?
+ O que faz no seu tempo livre?
+ Que livro está lendo?
+ Que tipo de livro você gosta?
+ Que tipo de filme você gosta?
+ Que tipo de música você gosta?
+ Qual seu cantor favorito?
+ Qual sua música favorita?
+ Gosta de samba?
+ Onde você gosta de ir?
+ Que livraria?
+ Que cinema você vai?
+ dentre outras... dê uma olhada na classe ProcessarMensagem.

Durante a execução do Chat, quando o usuário final envia uma mensagem, o sistema converte ela para caracteres minúsculos, remove acentos e caracteres especiais, depois remove os espaços duplicados, para finalmente, realizar a busca da mensagem (pergunta) através de expressões regulares.

Quando o Chat não entende a pergunta, ou seja, a mensagem enviada, responde para o usuário '*Não entendi o que você disse*'.

# Começando

As instruções abaixo irão te auxiliar para que você tenha uma cópia do **ChatSimples** na sua máquina.

## Pré-requisitos

1. Realize o download e instale [Open JDK 8](http://openjdk.java.net/install/), ou posterior.

## Instalando

1. Realize o download do arquivo XXX (todas as plataformas)
2. Descompacte o arquivo
3. Execute java -jar dist/ChatSimples.jar

## Executando o código fonte

1. Clone o repositório
2. Importe o projeto para sua IDE (Netbeans, Eclipse, IntelliJ)
3. Execute /br/com/drsolutions/chatsimples/main/ChatSimples.java

# Release Notes

Próximos passos:
+ Integrar o ChatSimples com o banco de dados SQLite
+ Integrar o ChatSimples com os bancos de dados PostgreSQL e MySQL
+ Fazer com que o bot consiga dar continuidade em conversas, como por exemplo, caso o usuário final começe a realizar perguntas de um determinado assunto, o bot consiga continuar enviando respostas dentro desse contexto

# Autor

**Diego Mendes Rodrigues** (São Paulo, Brasil)

Caso você precise de suporte para customização ou para uma licença comercial, fique a vontade para entrar em contato comigo.
 + E-mail: diego#drsolutions.com.br

# Licença

Copyright [2018] [Drsolutions Tecnologia em Informática Ltda.-ME]

Licensed under the Apache License, Version 2.0 (the "License");

you may not use this file except in compliance with the License.

You may obtain a copy of the License at

<http://www.apache.org/licenses/LICENSE-2.0>

Unless required by applicable law or agreed to in writing, software

distributed under the License is distributed on an "AS IS" BASIS,

WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

See the License for the specific language governing permissions and

limitations under the License.